@echo off

REM Usage
REM ==============
REM ./build app   : Builds exe and puts inside of out/
REM ./build clean : Deletes out/ folder
REM ./build cloc  : calls cloc on src code that isn't 3rd party

REM You can pass multiple args to do multiple things
REM ==============


REM todo: Get current directory of the script and use that so I can call the
REM the script from anywhere.

for %%a in (%*) do set "%%a=1"

if "%clean%" == "1" (
    rmdir /s /q "out"
)

if "%app%" == "1" (
    if not exist "out" (
      mkdir "out"
   )

    clang++ -std=c++17 -O0 -fno-rtti -fno-exceptions -Wall -Wno-unused-function -Wno-writable-strings -g -luser32 -lkernel32 code/saoirse_win32_main.cpp -o out/yk.exe
)