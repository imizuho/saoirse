/* date = April 28th 2024 7:00 am */

#ifndef BASE_COMMON_H
#define BASE_COMMON_H

#if defined(_WIN32)
#define OS_WIN32
#elif defined (__linux__)
#define OS_LINUX
#elif defined(__APPLE__)
#define OS_APPLE
#endif

#include <stdint.h>

#define ENABLE_ASSERTS 1

#define _Assert_helper(expr)                         \
do                                               \
{                                                \
if (!(expr))                                 \
{                                            \
volatile int *ptr = 0;                   \
*ptr = 0;                                \
}                                            \
} while (0)

#if ENABLE_ASSERTS
#define Assert(expr) _Assert_helper(expr)
#else
#define Assert(expr)
#endif

#define AlwaysAssert(expr) _Assert_helper(expr)

#define INVALID_CODE_PATH() { volatile int *ptr = 0; *ptr = 0; }

#define NOT_IMPLEMENTED() { volatile int *ptr = 0; *ptr = 0; }

#define Kilobytes(Value) ((uint64_t)(Value) * 1024)
#define Megabytes(Value) (Kilobytes(Value) * 1024)
#define Gigabytes(Value) (Megabytes(Value) * 1024)
#define Terabytes(Value) (Gigabytes(Value) * 1024)

#define internal static
#define global static
#define local_persist static

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef float f32;
typedef double f64;

typedef int32_t b32;

struct Arena
{
  size_t size;
  u8* base;
  size_t used;
};

struct Arena_temp
{
  Arena *arena;
  size_t pos;
};

internal void* _arena_alloc(Arena* arena, size_t size)
{
  Assert(arena->used + size <= arena->size);
  
  void* out = arena->base + arena->used;
  arena->used += size;
  return out;
}

#define push_struct(arena, type) (type*)_arena_alloc(arena, sizeof(type))

#define push_array(arena,type,count) (type*)_arena_alloc(arena, sizeof(type) * count)
internal Arena_temp arena_temp_begin(Arena *arena)
{
  Arena_temp out = {
    .arena = arena,
    .pos = arena->used,
  };
  return out;
}

internal void arena_temp_end(Arena_temp *temp)
{
  temp->arena->used = temp->pos;
}

internal void arena_innit(Arena* arena, size_t size, void* base)
{
  arena->size = size;
  arena->base = (u8*)base;
  arena->used = 0;
}

#if defined _WIN32
#define YK_API __declspec(dllexport)
#else
#define YK_API
#endif

#endif //BASE_COMMON_H