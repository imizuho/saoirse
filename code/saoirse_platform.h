/* date = May 15th 2024 0:54 am */

#ifndef SAOIRSE_PLATFORM_H
#define SAOIRSE_PLATFORM_H

#define LOG_BUFFER_SIZE Megabytes(1)

global char *buffer;
global size_t buffer_offset;

internal void print(const char *fmt, ...);


#endif //SAOIRSE_PLATFORM_H
