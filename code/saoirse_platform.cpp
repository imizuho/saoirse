void print(const char *fmt, ...)
{
  char *curr = buffer + buffer_offset;
  
  va_list args;
  va_start(args, fmt);
  
  int res = stbsp_vsnprintf(curr, LOG_BUFFER_SIZE - buffer_offset, fmt, args);
  va_end(args);
  
  buffer_offset += res;
  
  OS_print_console(curr, res);
}