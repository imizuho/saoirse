/* date = April 30th 2024 0:27 am */

#ifndef OS_H
#define OS_H

// make file path utility functions
// one to get application directory
// will be worked on as I need more

internal b32 OS_allocate(void *ptr, size_t size);
internal void OS_print_console(char *buffer, int size);

// ty yeti
internal size_t OS_get_app_dir(char *buffer, int size);

#if defined(OS_WIN32)
#include <windows.h>
internal b32 OS_allocate(void *ptr, size_t size)
{
  b32 res = (VirtualAlloc(ptr, size, 
                          MEM_RESERVE | MEM_COMMIT, 
                          PAGE_READWRITE) != 0);
  return res;
}

void OS_print_console(char *buffer, int size)
{
  WriteConsoleA(GetStdHandle(STD_OUTPUT_HANDLE), buffer, size, 0, 0);
}

// also, idea - always assume memory is zeroed out.
// that way you never have to care about null terminators
// still need to watch out for buffer overruns.

// actually, when i work on strings. i can make zeroing them out
// a requirement.

// when making the pf abstraction, return shit like DWORD
size_t OS_get_app_dir(char *buffer, int size)
{
  DWORD len = GetModuleFileName(0, buffer, size);
  char *c = &buffer[len];
  while(*(--c) != '\\')
  {
    *c = 0;
    --len;
  }
  
  buffer[len] = 0;
  return len;
}

#else
#include <unistd.h>
void OS_print_console(char *buffer, int size)
{
  write(STDOUT_FILENO, buffer, size);
}

size_t OS_get_app_dir(char *buffer, int size)
{
  ssize_t len = readlink("/proc/self/exe", buffer, size);
  char *c = &buffer[len];
  while(*(--c) != '/')
  {
    *c = 0;
    --len;
  }
  
  buffer[len] = 0;
  return len;
}

internal b32 OS_allocate(void *ptr, size_t size)
{
  NOT_IMPLEMENTED();
  
  return 0;
}

#endif

#endif //OS_H